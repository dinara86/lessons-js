// 22/06/2020 - First Page
// https://medium.com/javascript-in-plain-english/comparing-different-ways-to-make-http-requests-in-javascript-39ab0f090788

// Working with API requests  xhr, fetch, axios

const requestUrl = "https://jsonplaceholder.typicode.com/todos";

// FETCH

// function makeRequest(method, url, body = null) {
//     // return new Promise((resolve, reject) => {});
//
//     const headers = {
//        "Content-Type": "application/json"
//     }
//
//     return fetch(url, {
//         method: method,
//         body: JSON.stringify(body),
//         headers: headers
//     }).then(res => {
//             if(res.ok) {
//                 return res.json();
//             }
//
//             return res.json().then(error => {
//                 const e = new Error("Something went wrong");
//                 e.data = error;
//                 throw e;
//             });
//         });
//         // .then(res => res.json())
//         // .then(data => data);
// }
// GET method
// makeRequest("GET", requestUrl, null)
//     .then(data => console.log(data))
//     .catch(err => console.log(err));
// const body = {
//     name: "Dinara",
//     hairColor: "brown"
// }
//
// // POST method
//
// makeRequest("POST", requestUrl, body)
//     .then(res => console.log(res))
//     .catch(err => console.log(err));

// axios - tomorrow

// xhr -
